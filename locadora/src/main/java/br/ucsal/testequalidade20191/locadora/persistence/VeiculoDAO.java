package br.ucsal.testequalidade20191.locadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.testequalidade20191.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20191.locadora.exception.VeiculoNaoEncontradoException;

public class VeiculoDAO {

	private List<Veiculo> veiculos = new ArrayList<>();

	public Veiculo obterPorPlaca(String placa) throws VeiculoNaoEncontradoException {
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getPlaca().equalsIgnoreCase(placa)) {
				return veiculo;
			}
		}
		throw new VeiculoNaoEncontradoException();
	}
	
	public void insert(Veiculo veiculo){
		veiculos.add(veiculo);
	}

}
